#!/usr/bin/python
# -*- coding: utf-8 -*- 
from mongoengine import *
import hashlib

class Segment(Document):
    #parent_audio = StringField()
    segment_id = StringField(primary_key=True) 
    segment_audio = StringField()
    path = StringField()
    start_time = FloatField()# in seconds
    end_time = FloatField()# in seconds
    transcript = StringField()
   
    def __unicode__(self):
        if self.transcript:
            return ', '.join((self.segment_id, self.segment_audio, self.transcript))
        else:
            return ', '.join((self.segment_id, self.segment_audio))

    def dictInput(self, dic):
        '''
        dic needs to have a keys:
        start
        end
        file_ext
        transcript
        path
        '''
        
        self.segment_audio = dic['segment']
        self.segment_id = hashlib.md5(dic['segment'].encode('utf8')).hexdigest()
        self.start_time = float(dic['start'])
        self.end_time = float(dic['end'])
        self.transcript = dic.get('transcript')
        self.save()
       
    def get(self, segment_name):
        '''
        segment name is the filename of the segment. could come with the path information.
        extension and the path needs to be stripped
        '''
        
