import os
import sys
import subprocess
import io_tools
from optparse import OptionParser
from math import floor
from datetime import timedelta 

import speech_recognition as sr
from segment_db import Segment
from mongoengine import *
import hashlib

WIT_AI_KEY = os.environ['WIT_AI_KEY']
project_dir = os.path.dirname(os.path.realpath(__file__))
annotations = ['empty','music','language']
audio_tools = ['avconv', 'ffmpeg']

AUDIO_PATH=""
PROJECT_PATH=""

def DbConnect(dbname):
    mongo_db = dbname
    mongo_host = 'localhost'
    mongo_port = 27017
    alias = 'default'
    
    connect(mongo_db, alias=alias)
    
def cueInDictionary(data):
    cues = []
    for i in range(len(data)):
        if i == 0:
            start = 0.0
            annotation = ''
        else:
            start = float(data[i-1][0])
            annotation = data[i-1][1]
        end = float(data[i][0])
        #out = '_%03d.wav'%i
        ext = "%.3f_%.3f"%(start,end)
        dic = {}
        dic = {"start":start, "end":end, "file_ext":ext}
        dic['start_td'] = str(timedelta(seconds=dic['start']))
        dic['end_td'] = str(timedelta(seconds=dic['end']))
        if annotation in annotations:
            dic['annotation'] = annotation
        else:
            dic['annotation'] = ''
        cues.append(dic)
    return cues

def segmentFile(audio, structured_cues):
    audio_tool = audio_tools[0]
    if not audioToolExists(audio_tool):
        audio_tools = audio_tools[1]
    for cue in structured_cues:
        segmentCue(audio, cue, audio_tool=audio_tool)

def audioToolExists(audio_tool):
    try:
        subprocess.call([audio_tool,'--help'])
    except:
        return False

def segmentCue(audio, cue, audio_tool='avconv'):
        seek = floor(cue['start'])
        start = cue['start'] - seek
        end = cue['end']
        duration = end - cue['start']
        cue['segment'] = '_'.join((audio.split('.')[0],cue['file_ext']))#.replace(' ','_')
        cue['segment_path'] = os.path.join(PROJECT_PATH, cue['segment'])+'.wav'
        cmd = "ffmpeg -ss %s -i %s -ss %s -t %s -ac 1 -ar 16000 %s"%(seek, audio, start, duration, cue['segment_path'])
        print(cmd)
        args = [audio_tool, '-ss', str(seek), '-i', audio, '-ss', str(start), \
                '-t', str(duration), '-ac', '1', '-ar', '16000', cue['segment_path']]
        if os.path.isfile(cue['segment_path']):
            print("%s already exists skipping"%cue['segment'])
        else:
            print(' '.join(args))
            subprocess.call(args)   
            if not os.path.isfile(cue['segment_path']):
                raise IOError("File not created from ffmpeg(avconv) operation"
                              " %s"%cue['segment_path'])

def witAiRecognize(segment, WIT_ATI_KEY):
    r = sr.Recognizer()
    with sr.AudioFile(segment) as source:
        audio = r.record(source) # read the entire audio file

    result = None
    try:
        result = r.recognize_wit(audio, key=WIT_AI_KEY)
        print("*wit.ai* " + result)
    except sr.UnknownValueError:
        print("Wit.ai could not understand audio")
    except sr.RequestError as e:
        print("Could not request results from Wit.ai service; {0}".format(e))

    return result

def ibmRecognize(segment, language='en-UK'):
    r = sr.Recognizer()
    with sr.AudioFile(segment) as source:
        audio = r.record(source) # read the entire audio file
    
    IBM_PASSWORD = os.environ['WATSON_PASS']
    IBM_USER = os.environ['WATSON_USER']
    result = None
    try:
        result = r.recognize_ibm(audio, username=IBM_USER, password=IBM_PASSWORD, language=language)
        print("*watson* " + result)
    except sr.UnknownValueError:
        print("Watson could not understand audio")
    except sr.RequestError as e:
        print("Could not request results from Watson service; {0}".format(e))

    if result:
        result = result.replace('\n',' ')

    return result

def segmentTranscribe(options, cues):
    audio = options.audio
    audio_tool = audio_tools[0]
    if not audioToolExists(audio_tool):
        audio_tool = audio_tools[1]

    out_keys = ['start_td', 'end_td', 'transcript','segment', 'annotation']
    
    trans_out = audio.split('.')[0]+"_transcript.csv"

    audio_file = os.path.basename(audio)

    #TODO add a more through cleaning of the filename
    dbname = '.'.join(audio_file.split('.')[:-1]).replace(' ','_')
    print("opening db: %s"%dbname)
    DbConnect(dbname)
    for cue in cues:
        cue['segment'] = '_'.join((dbname,cue['file_ext']))

        # check if file exists in project dir
        cue['segment_path'] = os.path.join(PROJECT_PATH, cue['segment'])+'.wav'
        if not os.path.isfile(cue['segment_path']):
            print("audio segment %s does not exist"%cue['segment_path'])
            segmentCue(audio, cue, audio_tool=audio_tool)

        # check if file exists in db
        query = Segment.objects.filter(segment_audio=cue['segment'])
        if not query:
            print("segment %s not found in db"%cue['segment'])
            current_segment = Segment()
            current_segment.dictInput(cue)
        else:
            if len(query) > 1:
                print("ERROR: query yields more than one result")
                sys.exit()
            current_segment = query[0]

        # check if any annotation to not transcribe exists
        if not cue['annotation']:
            # check if transcript exists in db
            if not current_segment.transcript:
                print("sending %s for transcription:"%cue['segment'])
                if options.machine == 'wit.ai':
                    transcript = witAiRecognize(cue['segment_path'], WIT_AI_KEY)
                elif options.machine == 'ibm':
                    transcript = ibmRecognize(cue['segment_path'])
                else:
                    print("ERROR: unkown transcription machine")
                    sys.exit()
            else:
                transcript = current_segment.transcript
            if transcript:
                cue['transcript'] = transcript#.encode('utf8')
                current_segment.transcript = transcript#.encode('utf8')
                current_segment.save()
    io_tools.outCsv(out_keys, cues, trans_out)   
     
    
def readSegmentationCues(segmentation):
    '''
    format of the file should be a headerless csv with first column as seconds
    which is the default csv output of sonic visualizer
    '''
    data = [line.strip().split(',') for line in open(segmentation,"r+").readlines()]#TODO in io_tools 
    cues = []
    cues = cueInDictionary(data)
    return cues

def checkFile(filename, variable):
    if not filename:
        print("%s file not given"%variable)
        sys.exit()
    else:
        if not os.path.isfile(filename):
            print("%s file %s does not exist"%(variable, filename))
            sys.exit()
    
def main(options):
    checkFile(options.audio,"audio")
    checkFile(options.segmentation,"segmentation")

    AUDIO_PATH=options.audio
    PROJECT_PATH=os.path.dirname(AUDIO_PATH)

    cues = readSegmentationCues(options.segmentation)
    if options.transcribe:
        segmentTranscribe(options, cues)
    else:
        segmentFile(options.audio, cues)

if __name__ == "__main__":
    usage = "usage: %prog [-i infile] [option]"
    parser = OptionParser(usage=usage)
    parser.add_option("-a", "--audio", dest="audio", default=None, help="audio file to be segmented", type="string")
    parser.add_option("-s", "--segmentation", dest="segmentation", default=None, help="csv file with segmentation cues", type="string")
    parser.add_option("-t","--transcribe", dest="transcribe", action="store_true", default=False, help="send segments to wit.ai [default: %default]")
    parser.add_option("-m","--machine", dest="machine", default="wit.ai",type="string")
    (options, args) = parser.parse_args()



    main(options)
