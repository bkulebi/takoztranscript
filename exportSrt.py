import sys
import codecs

from datetime import timedelta
from math import floor

from segment_db import Segment 
from segment_audio import DbConnect

def timedelta2srt(td):
    '''
    inputs timedelta outputs srt formatted string
    '''
    if type(td) == timedelta:
        total_seconds = td.total_seconds()
    elif type(td) == float or type(td) == int:
        total_seconds = td
    else:
        print("total seconds has unknown type:", type(td))
        sys.exit()

    if total_seconds < 0:
        print("total seconds cannot be negative: %f"%total_seconds)
        sys.exit()
    elif total_seconds > 86400:
        print("total seconds cannot be larger than a day: %f"%total_seconds)
        sys.exit()
    else:
        hours = floor(total_seconds/60./60.)
        mod_hours = total_seconds%(60.*60.)
        minutes = floor(mod_hours/60.)
        seconds = mod_hours%60.
        return ('%02d:%02d:%06.3f'%(hours, minutes, seconds)).replace('.',',')

def exportSrt(dbname):
    DbConnect(dbname)
    cues = Segment.objects.order_by('start_time')

    f = codecs.open(dbname+".srt", 'w','utf8')
    for i in range(len(cues)):
        line = i+1
        cue = cues[i]
        start = timedelta2srt(cue.start_time)
        end = timedelta2srt(cue.end_time)
        text = cue.transcript
        f.write('%s\n'%line)
        f.write('%s --> %s\n'%(start,end))
        f.write('%s\n\n'%text)
    f.close()

if __name__ == "__main__":
    dbname = sys.argv[1]
    exportSrt(dbname)
    
